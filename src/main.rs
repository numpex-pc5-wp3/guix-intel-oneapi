use chumsky::prelude::*;
use futures::StreamExt;
use reqwest::{Method, Request, Url};
use serde::Serialize;
use std::collections::hash_map::Entry;
use std::collections::BinaryHeap;
use std::io::{BufRead, Write};
use std::path::PathBuf;
use std::process::Command;
use std::time::{Duration, Instant};
use std::{
    collections::HashMap,
    fs::{self, File},
    io::BufWriter,
    ops,
};
use tokio::io::AsyncWriteExt;
use tokio::task::{self, JoinSet};
use tower::{Service, ServiceExt};

#[derive(Debug, Serialize, PartialEq, PartialOrd, Eq, Ord)]
struct Package {
    // Order matters for Ord derive
    version: String,
    package: String,
    // Rest
    filename: String,
    description: String,
    depends: Vec<String>,
    md5: String,
}

mod test {
    use crate::parse_depends;
    use chumsky::Parser;

    #[test]
    fn depends() {
        let input = "intel-oneapi-common-vars (>= 2024.1.0-0), intel-oneapi-common-licensing-2024.1, intel-oneapi-common-oneapi-vars-2024.1";
        let res = parse_depends().parse(input);

        let expected = [
            "intel-oneapi-common-vars",
            "intel-oneapi-common-licensing-2024.1",
            "intel-oneapi-common-oneapi-vars-2024.1",
        ]
        .into_iter()
        .map(String::from)
        .collect();

        assert_eq!(res, Ok(expected));
    }
}

fn parse_depends() -> impl Parser<char, Vec<String>, Error = Simple<char>> {
    let text = filter(|c: &char| !c.is_whitespace() && !matches!(*c, '(' | ')' | ','))
        .repeated()
        .at_least(1)
        .padded();

    let ident = text.padded().collect::<String>();

    let version = text
        .padded()
        .repeated()
        .at_least(1)
        .delimited_by(just('('), just(')'))
        .repeated(); // repeated to parse 0 or 1 time

    ident.then_ignore(version).separated_by(just(','))
}

fn parse_block() -> impl Parser<char, Vec<Package>, Error = Simple<char>> {
    let to_line_end = filter(|c: &char| *c != '\n')
        .repeated()
        .at_least(1)
        .delimited_by(just(' '), just('\n'))
        .collect::<String>();

    let ident = filter(|c: &char| c.is_alphanumeric() || *c == '-')
        .repeated()
        .at_least(1)
        .collect::<String>();

    let line = ident.then_ignore(just(':')).then(to_line_end);

    let block = line
        .repeated()
        .at_least(1)
        .collect::<HashMap<_, _>>()
        .try_map(|mut x, span: ops::Range<usize>| {
            let mut take = |name, span| {
                x.remove(name)
                    .ok_or_else(|| Simple::custom(span, format!("Didn't find field: {}", name)))
            };

            let depends = take("Depends", span.clone())?;
            let res = parse_depends()
                .parse(depends)
                .unwrap_or_else(|_| vec![String::from("PARSE_ERROR")]);

            Ok(Package {
                package: take("Package", span.clone())?,
                version: take("Version", span.clone())?,
                filename: take("Filename", span.clone())?,
                description: take("Description", span.clone())?,
                depends: res,
                md5: take("MD5sum", span.clone())?,
            })
        });

    block.padded().repeated().at_least(2)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let path = "response.txt";

    let body = if let Ok(s) = fs::read_to_string(path) {
        s
    } else {
        {
            let mut file = File::options().write(true).create(true).open(path)?;

            for url in [
                "https://apt.repos.intel.com/oneapi/dists/all/main/binary-amd64/Packages",
                "https://apt.repos.intel.com/oneapi/dists/all/main/binary-all/Packages",
            ] {
                let body = reqwest::get(url).await?.bytes().await?;
                file.write_all(&body)?;
            }
        }

        fs::read_to_string(path)?
    };

    let now = Instant::now();
    let all_packages = parse_block().parse(body).unwrap();
    eprintln!("took {:?}", now.elapsed());

    let mut packages_merged: HashMap<String, BinaryHeap<&Package>> = HashMap::new();

    for package in all_packages.iter() {
        let name = package.package.clone();
        match packages_merged.entry(name) {
            Entry::Occupied(mut x) => {
                let coll = x.get_mut();
                coll.push(package);
            }
            Entry::Vacant(x) => {
                let mut coll = BinaryHeap::new();
                coll.push(package);
                x.insert(coll);
            }
        }
    }

    let output = BufWriter::new(
        File::options()
            .truncate(true)
            .write(true)
            .open("result.txt")?,
    );
    serde_json::to_writer_pretty(output, &packages_merged)?;

    let cache_dir = PathBuf::from(
        std::env::var("XDG_CACHE_HOME")
            .unwrap_or_else(|_| format!("{}/.cache", std::env::var("HOME").unwrap())),
    )
    .join("guix-oneapi");
    std::fs::create_dir_all(&cache_dir)?;

    let mut joinset = JoinSet::new();

    let client = reqwest::Client::new();
    let svc = tower::ServiceBuilder::new()
        .buffer(100)
        .concurrency_limit(10)
        // .rate_limit(1, Duration::from_secs(1))
        .service(client);

    for package in all_packages.iter() {
        let p = PathBuf::from(&package.filename);

        let path = cache_dir.join(p.file_name().unwrap()).to_owned();
        let url = Url::parse(&format!(
            "https://apt.repos.intel.com/oneapi/{}",
            package.filename
        ))?;

        let mut svc = svc.clone();
        let md5 = package.md5.clone();
        let name = package.package.clone();

        joinset.spawn(async move {
            if !path.exists() {
                eprintln!("Downloading {:?}", path);
                let req = Request::new(Method::GET, url);
                let resp = svc.ready().await.unwrap().call(req).await.unwrap();
                let status = resp.status();
                if !status.is_success() {
                    panic!("{:?}", status);
                }

                let mut file = tokio::fs::File::options()
                    .create(true)
                    .truncate(true)
                    .write(true)
                    .open(&path)
                    .await
                    .unwrap();

                let mut body_stream = resp.bytes_stream();
                while let Some(chunk) = body_stream.next().await {
                    let chunk = chunk.unwrap();
                    file.write(&chunk).await.unwrap();
                }
                eprintln!("Download finished for {:?}", path);
            } else {
                eprintln!("Reusing {:?}", path);
            }

            let md5_hex = format!("{:x}", md5::compute(tokio::fs::read(&path).await.unwrap()));
            if md5 != md5_hex {
                panic!("Hash mismatch {:?}", path);
            }

            task::spawn_blocking(move || {
                let cmd = Command::new("guix")
                    .arg("download")
                    .arg(&path)
                    .output()
                    .unwrap();

                let mut lines = cmd.stdout.lines();
                lines.next();
                let hash = lines.next().unwrap().unwrap();
                eprintln!("{:?} -> {}", path, hash);
                (name, hash)
            })
        });
    }

    let mut result_file = File::options()
        .create(true)
        .truncate(true)
        .write(true)
        .open("hashes.scm")
        .unwrap();

    writeln!(&mut result_file, "(define-public hashes '(")?;

    while let Some(next) = joinset.join_next().await {
        let (name, hash) = next?.await?;

        writeln!(&mut result_file, "  (\"{}\" . \"{}\")", name, hash)?;
    }

    writeln!(&mut result_file, "))")?;

    Ok(())
}
