with import <nixpkgs> {};
  mkShell {
    packages = [
      rustc
      cargo
      rust-analyzer-unwrapped
      rustfmt
      clippy

      (python3.withPackages (p: [
        p.python-apt
      ]))
    ];

    shellHook = ''
      venv="$(cd $(dirname $(which python)); cd ..; pwd)"
      ln -Tsf "$venv" .venv
    '';

    env.RUST_SRC_PATH = "${rustPlatform.rustLibSrc}";
  }
